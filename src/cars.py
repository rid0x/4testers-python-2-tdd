from datetime import datetime

class Car:
    def __init__(self, brand, color, production_year):
            self.brand = brand
            self.color = color
            self.production_year = production_year
            self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        now = datetime.now().year
        return now - self.production_year
    
    def repaint(self, color):
        self.color = color


if __name__ == '__main__':
    car1 = Car("Toyota", "red", 2000)
    car2 = Car("Ford", "blue", 2010)
    car3 = Car("BMW", "black", 2020)


car1.repaint("black")
car1.drive(1000)

print(car1.mileage)
print(car1.color)
print(car1.get_age())
