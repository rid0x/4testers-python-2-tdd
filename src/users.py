class User:
    def __init__(self, email, name):
        self.email = email
        self.name = name

    def say_hello(self):
        print(f'Hey! My name is {self.name}')


if __name__ == '__main__':
    kate = User('kate@example.com', 'Kate')
    john = User('john@example.com', 'John')

    print(f'Hey! My name is {kate.name}, and my email is {kate.email}')
    print(f'Hey! My name is {john.name}, and my email is {john.email}')

    kate.say_hello()
    john.say_hello()
