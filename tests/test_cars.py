from src.cars import Car
def test_initial_state_of_a_car():
    car = Car('Toyota', 'red', 2000)
    assert car.brand == 'Toyota'
    assert car.color == 'red'
    assert car.production_year == 2000
    assert car.mileage == 0

def test_drive_car_once():
    car = Car('Toyota', 'red', 2000)
    car.drive(100)
    assert car.mileage == 100

def test_drive_car_twice():
    car = Car('Toyota', 'red', 2000)
    car.drive(100)
    car.drive(100)
    assert car.mileage == 200


def test_get_age_of_car_correctly():
    car = Car('Toyota', 'red', 2000)
    car.get_age()
    assert car.get_age() == 23

def test_get_age_of_car_incorrectly():
    car = Car('Toyota', 'red', 2000)
    car.get_age()
    assert not car.get_age() == 23

def test_repainting_of_car_into_black():
    car = Car('Toyota', 'red', 2000)
    car.repaint('black')
    assert car.color == 'black'

def test_repainting_of_car_into_black():
    car = Car('Toyota', 'red', 2000)
    car.repaint('black')
    assert not car.color == 'red'
