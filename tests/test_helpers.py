from src.helpers import is_person_adult

def test_is_not_adult_for_age_17():
    is_adult = is_person_adult(17)
    assert not is_adult

def test_is_adult_for_age_18():
    is_adult = is_person_adult(18)
    assert is_adult

def test_is_adult_for_age_19():
    is_adult = is_person_adult(19)
    assert is_adult
